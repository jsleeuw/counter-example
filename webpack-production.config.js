var webpack = require('webpack');
var path = require('path');

var buildPath = path.resolve(__dirname, 'build');
var nodeModulesPath = path.resolve(__dirname, 'node_modules');
var TransferWebpackPlugin = require('transfer-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');

var appSrc = path.resolve(__dirname, 'src');

var config = {
  devtool: 'cheap-module-source-map',
  entry: [path.join(__dirname, '/src/index.js')],
  output: {
    path: buildPath,    //Path of output file
    filename: 'app.js'  //Name of output file
  },
  resolve: {
    extensions: ["", ".js"]
  },
  resolveLoader: {
    root: nodeModulesPath,
    moduleTemplates: ['*-loader']
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/, //All .js and .jsx files
        include: appSrc,
        loader: 'babel', //react-hot is like browser sync and babel loads jsx and es6-7
      },
      {
        test: /\.scss$/,
        include: appSrc,
        loaders: ['style', 'css', 'resolve-url', 'sass?sourceMap']
      },
      {
        test: /\.css$/,
        include: appSrc,
        // loader: 'style!css!postcss',
        loader: ExtractTextPlugin.extract('style', 'css?-autoprefixer!postcss')
      },
      {
        test: /\.svg$/,
        include: appSrc,
        loader: 'file'
      }
    ]
  },
  eslint: {
    configFile: '.eslintrc'
  },
  postcss: function() {
    return [autoprefixer];
  },
  plugins: [
    new webpack.DefinePlugin({ 'process.env.NODE_ENV': '"production"' }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        screw_ie8: true,
        warnings: false
      },
      mangle: {
        screw_ie8: true
      },
      output: {
        comments: false,
        screw_ie8: true
      }
    }),
    new ExtractTextPlugin('[name].[contenthash].css'),
    //Allows error warnings but does not stop compiling. Will remove when eslint is added
    new webpack.NoErrorsPlugin(),
    //Transfer Files
    new TransferWebpackPlugin([
      {from: 'www'}
    ], path.resolve(__dirname,"src"))
  ],


};

module.exports = config;
