import React from 'react'
import ReactDOM from 'react-dom'
import {observer, Provider} from 'mobx-react'
import {observable, action} from 'mobx'
import {AppStore} from './stores/AppStore'
import App from './App'

import './www/styles/sass/main.scss'

const store = new AppStore()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
