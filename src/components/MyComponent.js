import React, { Component, PropTypes } from 'react'
import { observer, inject } from 'mobx-react'

@inject("store") @observer
export default class MyComponent extends Component {
  render() {
    const { store } = this.props
    return <div>
        <div>{store.decoratedCounter}</div>
        <button onClick={() => store.increase()}>+</button>
        <button onClick={() => store.decrease()}>-</button>
      </div>
  }
}
