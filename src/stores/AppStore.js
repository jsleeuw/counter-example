import {observable, action, computed, autorun, transaction, toJS} from "mobx"

export class AppStore {
  @observable counter = 0

  @computed get decoratedCounter() {
    return `we have a count of ${this.counter}`
  }

  @action increase(){
    console.log('fdksldjkf')
    this.counter++
  }

  @action decrease(){
    this.counter--
  }
}
