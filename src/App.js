import React, { Component, PropTypes } from 'react'
import MyComponent from './components/MyComponent'

export default class App extends Component {
  render() {
    console.log(`we are running in ${process.env.NODE_ENV}`)
    return (
      <div className="container">
        <MyComponent />
      </div>
    )
  }
}
