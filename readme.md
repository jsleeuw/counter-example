A simple example project showing Webpack, Babel, React and mobx.

* To **install**: `npm install`
* To run in **dev mode**: `npm start`
* To **build**: `npm run build`
