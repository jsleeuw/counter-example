var webpack = require('webpack');
var path = require('path');
var buildPath = path.resolve(__dirname, 'build');
var nodeModulesPath = path.resolve(__dirname, 'node_modules');
var TransferWebpackPlugin = require('transfer-webpack-plugin');
var autoprefixer = require('autoprefixer');

var appSrc = path.resolve(__dirname, 'src');

var config = {
  devtool: 'cheap-module-source-map',
  //Entry points to the project
  entry: [
    'react-hot-loader/patch',
    'webpack/hot/dev-server',
    'webpack/hot/only-dev-server',
    path.join(__dirname, '/src/index.js')
  ],
  output: {
    path: buildPath,    //Path of output file
    filename: 'app.js'
  },
  //Config options on how to interpret requires imports
  resolve: {
    extensions: ["", ".js"]
    //node_modules: ["web_modules", "node_modules"]  (Default Settings)
  },
  resolveLoader: {
    root: nodeModulesPath,
    moduleTemplates: ['*-loader']
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['babel'],
        include: path.join(__dirname, 'src')
      },
      {
        test: /\.scss$/,
        include: appSrc,
        loaders: ['style', 'css', 'resolve-url', 'sass?sourceMap']
      },
      {
        test: /\.css$/,
        include: appSrc,
        loader: 'style!css!postcss'
      },
      {
        test: /\.svg$/,
        include: appSrc,
        loader: 'file'
      }
    ]
  },
  eslint: {
    configFile: '.eslintrc'
  },
  postcss: function() {
    return [autoprefixer];
  },
  devServer:{
    contentBase: 'src/www',  //Relative directory for base of server
    devtool: 'eval',
    hot: true,        //Live-reload
    inline: true,
    port: 8000,        //Port Number
    host: 'localhost'  //Change to '0.0.0.0' for external facing server
  },
  plugins: [
    //Enables Hot Modules Replacement
    new webpack.HotModuleReplacementPlugin(),
    //Allows error warnings but does not stop compiling. Will remove when eslint is added
    new webpack.NoErrorsPlugin(),
    //Moves files
    new TransferWebpackPlugin([
      {from: 'www'}
    ], path.resolve(__dirname, "src"))
  ],
};

module.exports = config;
